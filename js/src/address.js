exports.formatAddress = function (address) {
  var str = "";
  if (address.streetNumber) {
    str += address.streetNumber + " ";
  }
  if (address.street) {
    str += address.street + "\n";
  }
  if (address.suburb) {
    str += address.suburb;
    if (address.state) {
      str += " " + address.state;
    }
    if (address.postcode) {
      str += " " + address.postcode;
    }
  }
  if (address.country) {
    str += "\n" + address.country;
  }
  return str;
}
