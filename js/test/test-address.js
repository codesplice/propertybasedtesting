const formatter = require('../src/address.js');
const assert = require('chai').assert;
const jsc = require('jsverify');

describe('formatAddress', function() {
  
  it('should throw if passed undefined', function() {
    assert.throws(() => formatter.formatAddress(undefined), TypeError);
  });
  
  it('should format the address correctly', function() {
    assert.equal("123 Main Road\nBig City", formatter.formatAddress({ streetNumber: 123, street: "Main Road", suburb: "Big City" }));
    assert.equal("Main Road\nBig City WA 6000", formatter.formatAddress({ street: "Main Road", suburb: "Big City", state: "WA", postcode: 6000 }));
    assert.equal("123 Main Road\nBig City\nAustralia", formatter.formatAddress({ streetNumber: 123, street: "Main Road", suburb: "Big City", country: "Australia" }));
    assert.equal("123 Main Road\nBig City WA 6000\nAustralia", formatter.formatAddress({ streetNumber: 123, street: "Main Road", suburb: "Big City", state: "WA", postcode: 6000, country: "Australia" }));
  });

});