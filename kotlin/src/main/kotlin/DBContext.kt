package au.com.codesplice.portablesupernaturalanimals

class DBContext(var monsters: List<Monster> = listOf(), var friends: List<Friend> = listOf()) {
  fun collect(new: Monster) {
    val new = new.copy()
    monsters = monsters + new
  }

  fun get(name: String): Monster? {
    return monsters.find({ it.name == name})
  }

  fun addFriend(friend: Friend) {
    val friend = friend.copy(email = friend.email.normaliseEmail())
    friends = friends + friend
  }

  fun isFriend(email: String): Boolean {
    return friends.find { it.email == email } != null
  }

  fun moveToGym(gym: Gym, monster: Monster) {
    gym.addMonster(monster)
    monsters = monsters.minus(monster)
  } 

  override fun toString(): String {
    return "DBContext(\nmonsters: [${monsters.map { it.toString() }.joinToString("\n")}]\n" + 
      "friends: [${friends.map { it.toString() }.joinToString("\n")}]"
  }

  fun copy(): DBContext {
    return DBContext(monsters, friends)
  }

  override fun equals(other: Any?): Boolean {
    val otherCtx = other as? DBContext 
    if (otherCtx != null) {
      return monsters == otherCtx.monsters && friends == otherCtx.friends
    } else {
      return false
    }
  }
}
