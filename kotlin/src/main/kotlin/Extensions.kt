package au.com.codesplice.portablesupernaturalanimals

fun String.normaliseEmail(): String {
  return this.toLowerCase()
}
