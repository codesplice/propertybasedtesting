package au.com.codesplice.portablesupernaturalanimals

import com.github.salomonbrys.kotson.*
import com.google.gson.*

data class Monster(val type: String, var name: String, var hp: Int, var cp: Int) {
  constructor(json: JsonObject): this(json["type"].string, 
    json["name"].string, 
    json["hp"].nullInt ?: 0, 
    json["cp"].nullInt ?: 0) {}

  fun toJson(): JsonObject {
    return jsonObject(
      "type" to type,
      "name" to name,
      "hp" to hp 
    )
  }

  fun isFainted(): Boolean = hp == 0
}

data class Friend(val email: String, val name: String)

data class StoreItem(val description: String, val price: Int)

data class Gym(var team: String, var monsters: List<Monster>) {
  fun addMonster(monster: Monster) {
    if (!monster.isFainted()) {
      monsters = monsters + monster
    }
  }

  fun removeMonster(monster: Monster) {
    monsters = monsters.minus(monster)
  }
}