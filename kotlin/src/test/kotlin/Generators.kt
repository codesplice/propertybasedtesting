package au.com.codesplice.portablesupernaturalanimals

import io.kotlintest.properties.Gen

val types = listOf(
  "Bulbasaur",
  "Ivysaur",
  "Venusaur",
  "Charmander",
  "Charmeleon",
  "Charizard",
  "Squirtle",
  "Wartortle",
  "Blastoise",
  "Caterpie",
  "Metapod",
  "Butterfree",
  "Weedle",
  "Kakuna",
  "Beedrill",
  "Pidgey",
  "Pidgeotto",
  "Pidgeot",
  "Rattata",
  "Raticate"
)

class MonsterGenerator : Gen<Monster> {
  override fun generate(): Monster = Monster(Gen.oneOf(types).generate(), 
  Gen.string().generate(), 
  Gen.choose(0, 200).generate(),
  Gen.choose(0, 200).generate())
}

class DBContextGenerator : Gen<DBContext> {
  override fun generate(): DBContext = DBContext(listOf(), listOf()) 
}

class FriendGenerator : Gen<Friend> {
  override fun generate(): Friend = Friend(Gen.string().generate(), Gen.string().generate())
}

class GymGenerator : Gen<Gym> {
  override fun generate(): Gym = Gym(Gen.oneOf(listOf("Chastity", "Frugality", "Sobriety")).generate(), listOf())
}