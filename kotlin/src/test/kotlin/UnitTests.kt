package au.com.codesplice.portablesupernaturalanimals

import kotlin.test.*
import org.junit.Test
import io.kotlintest.specs.*
import io.kotlintest.properties.Gen
import com.github.salomonbrys.kotson.*
import com.google.gson.*

class MonsterTests {
    @Test fun toJson() {
      val monster = Monster("Bulbasaur", "That flowery thing", 10, 20)
      val json = monster.toJson()
      assertEquals("Bulbasaur", json["type"].string)
      assertEquals("That flowery thing", json["name"].string)
      assertEquals(10, json["hp"].int)
    }

    @Test fun fromJson() {
      val json = jsonObject(
        "type" to "Butterfree",
        "name" to "I can’t believe it’s not",
        "hp" to 12,
        "cp" to 23)
      val monster = Monster(json)
      assertEquals("Butterfree", monster.type)
      assertEquals("I can’t believe it’s not", monster.name)
      assertEquals(12, monster.hp)
      assertEquals(23, monster.cp)
    }
}

class DBContextTests {

    @Test fun addFriend() {
      val ctx = DBContext()
      ctx.addFriend(Friend("ash@oaklabs.edu", "Ash"))
      assertTrue(ctx.isFriend("ash@oaklabs.edu"))
      assertFalse(ctx.isFriend("alain@sycamorelabs.edu"))
    }

    @Test fun collectMonster() {
      val ctx = DBContext()
      val new = Monster("Bulbasaur", "That flowery thing", 10, 10)
      ctx.collect(new)
      assertTrue(ctx.monsters.contains(new))
    }

    @Test fun getExistingMonster() {
      val ctx = DBContext(listOf(
        Monster("Squirtle", "The wet one", 10, 5),
        Monster("Charmander", "The dragony one", 12, 8)
      ))
      val monster = ctx.get("The wet one")
      assertNotNull(monster)
    }

    @Test fun getDoesntExist() {
      val ctx = DBContext(listOf(
        Monster("Squirtle", "The wet one", 10, 5),
        Monster("Charmander", "The dragony one", 12, 8)
      ))
      val monster = ctx.get("The cool one that I don’t have yet")
      assertNull(monster)
    }

    @Test fun moveToGym() {
      val ctx = DBContext(listOf(
        Monster("Squirtle", "The wet one", 10, 5),
        Monster("Charmander", "The dragony one", 12, 8)
      ))
      val gym = Gym("Chastity", listOf())
      val monster = ctx.monsters[0]
      ctx.moveToGym(gym, monster)
      assertEquals(gym.monsters[0], monster)
      assertFalse(ctx.monsters.contains(monster))
    }
}


class PropertyBasedTests : StringSpec() {
  init {
    
  }
}
