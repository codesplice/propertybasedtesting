import PackageDescription

let package = Package(
  name: "WidgetProcessor",
  dependencies: [
    .Package(url: "https://github.com/typelift/SwiftCheck.git", majorVersion: 0),
  ]
)

