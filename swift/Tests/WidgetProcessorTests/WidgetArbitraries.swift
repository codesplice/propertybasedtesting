//
//  WidgetArbitraries.swift
//  SwiftCheckDemo
//
//  Created by Sam Ritchie on 2/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import SwiftCheck
@testable import WidgetProcessor

extension WidgetStatus: Arbitrary {
    public static var arbitrary: Gen<WidgetStatus> {
        return Gen<WidgetStatus>.fromElementsOf([.good, .bad, .ugly])
    }
}

extension Widget: Arbitrary {
    public static var arbitrary: Gen<Widget> {
        return Gen.compose {c in
            Widget(value: c.generate(), status: c.generate())
        }
    }
}

extension Doodad: Arbitrary {
    public static var arbitrary: Gen<Doodad> {
        return Gen.compose { c in
            return Doodad(isTransmogrified: c.generate(), 
                wimpleGlumpf: c.generate(using: Gen.oneOf([String.arbitrary, Gen.pure("")])))
        }
    }
}

extension Thingamajig: Arbitrary {
    public static var arbitrary: Gen<Thingamajig> {
        return Gen.compose { c in Thingamajig(frumpShizzle: c.generate()) }
    }
}
